let checkTime = (i) => {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
}

let startTime = () => {
    var today = new Date();
    var date = today.getFullYear()+' / '+(today.getMonth()+1)+' / '+today.getDate();

    document.getElementById('time').innerHTML = today.getHours() + ":" + checkTime(today.getMinutes()) + ":" + checkTime(today.getSeconds()) + ' ' + date;
    t = setTimeout(() => {
      startTime()
    }, 500);
}

startTime();
